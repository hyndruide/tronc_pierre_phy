/**
   --------------------------------------------------------------------------------------------------------------------
   Example sketch/program showing how to read data from more than one PICC to serial.
   --------------------------------------------------------------------------------------------------------------------
   This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid

   Example sketch/program showing how to read data from more than one PICC (that is: a RFID Tag or Card) using a
   MFRC522 based RFID Reader on the Arduino SPI interface.

   Warning: This may not work! Multiple devices at one SPI are difficult and cause many trouble!! Engineering skill
            and knowledge are required!

   @license Released into the public domain.

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS 1    SDA(SS)      ** custom, take a unused pin, only HIGH/LOW required *
   SPI SS 2    SDA(SS)      ** custom, take a unused pin, only HIGH/LOW required *
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15

*/

#include <SPI.h>
#include <MFRC522.h>
#include "Timer.h"


#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_1_PIN        8         // Configurable, take a unused pin, only HIGH/LOW required, must be diffrent to SS 2
#define SS_2_PIN        7          // Configurable, take a unused pin, only HIGH/LOW required, must be diffrent to SS 1
#define SS_3_PIN        6         // Configurable, take a unused pin, only HIGH/LOW required, must be diffrent to SS 2
#define SS_4_PIN        5          // Configurable, take a unused pin, only HIGH/LOW required, must be diffrent to SS 1


#define NR_OF_READERS   4

byte ssPins[] = {SS_1_PIN, SS_2_PIN, SS_3_PIN, SS_4_PIN,};
byte readers[4];
Timer tim;
byte byteCount;
byte buffer[18];

MFRC522 mfrc522[NR_OF_READERS];   // Create MFRC522 instance.
MFRC522::StatusCode status;
/**
   Initialize.
*/

void resetrf();
void setup() {

  Serial.begin(9600); // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  pinMode(4, OUTPUT);
  SPI.begin();        // Init SPI bus
  //tim.oscillate(3, 1000, HIGH);
  tim.every(400, resetrf);

  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN); // Init each MFRC522 card
    Serial.print(F("Reader "));
    Serial.print(reader);
    Serial.print(F(": "));
    mfrc522[reader].PCD_DumpVersionToSerial();
  }
}




/**
   Main loop.
*/
void loop() {


  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    // Look for new cards

    if (mfrc522[reader].PICC_IsNewCardPresent() && mfrc522[reader].PICC_ReadCardSerial()) {
      byteCount = sizeof(buffer);
      status = mfrc522[reader].MIFARE_Read(0x04, buffer, &byteCount);
      Serial.print("reader ");
      Serial.print(reader);
      Serial.print(" : ");
      Serial.println(buffer[0], HEX);
      readers[reader] = buffer[0];

      // Halt PICC
      mfrc522[reader].PICC_HaltA();
      // Stop encryption on PCD
      mfrc522[reader].PCD_StopCrypto1();
    } //if (mfrc522[reader].PICC_IsNewC
    //for(uint8_t reader

  } //for(uint8_t reader

  if (readers[0] == 0x11 && readers[1] == 0x22 && readers[2] == 0x33 && readers[3] == 0x44) {
    digitalWrite(4, HIGH);
  }

  tim.update();
}





void resetrf() {
    Serial.println("reset");
  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Reset();
    readers[reader] = 0;
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN);
    
  }

}



/**
   Helper routine to dump a byte array as hex values to Serial.
*/
byte dump_byte_array(byte *buffer, byte bufferSize) {
  Serial.println(bufferSize);
  for (byte i = 0; i < 20; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
